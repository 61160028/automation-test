<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Style the container for inputs .contain_85f58a</name>
   <tag></tag>
   <elementGuidId>1dff55c1-c979-43d8-8d32-128db8e745f4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-wrapper']/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-lg-12</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-lg-12</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                	
/* Style the container for inputs */
.container {
  background-color: #f1f1f1;
  padding: 20px;
}

/* The message box is shown when the user clicks on the password field */
#message {
  display:none;
  background: #f1f1f1;
  color: #000;
  position: relative;
  padding: 20px;
  margin-top: 10px;
}

#message p {
  padding: 10px 35px;
  font-size: 18px;
}

/* Add a green text color and a checkmark when the requirements are right */
.valid {
  color: green;
}

.valid:before {
  position: relative;
  left: -35px;
  content: &quot;✔&quot;;
}

/* Add a red text color and an &quot;x&quot; when the requirements are wrong */
.invalid {
  color: red;
}

.invalid:before {
  position: relative;
  left: -35px;
  content: &quot;✖&quot;;
}

เปลี่ยนรหัสผ่าน (Change Password)

        
        
            
            	
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                ไม่พบตัวอักษรภาษาอังกฤษ ในรหัสผ่านใหม่ของท่านNot found characters in your new password

                
                    Change Password
                
                
                
            
        
    
    





function myFunction() {
  var x = document.getElementById(&quot;newpass&quot;);
  var y = document.getElementById(&quot;renewpass&quot;);
  if (x.type === &quot;password&quot;) {
    x.type = &quot;text&quot;;
	y.type = &quot;text&quot;;
  } else {
    x.type = &quot;password&quot;;
	y.type = &quot;password&quot;;
  }
}

function check(event){
	var ew = event.which;
	
    if(33 &lt;= ew &amp;&amp; ew &lt;= 38)
        return true;
    if(40 &lt;= ew &amp;&amp; ew &lt;= 126)
        return true;
    if(ew == 39){
		alert(&quot;Don't use ' (single quote) ห้ามใช้เครื่องหมาย ' (เขาเดี่ยว)&quot;);
        return false;
	}
	alert(&quot;กรุณาเปลี่ยนภาษา&quot;);
    return false;
}

var myInput = document.getElementById(&quot;newpass&quot;);
var letter = document.getElementById(&quot;letter&quot;);
var capital = document.getElementById(&quot;capital&quot;);
var number = document.getElementById(&quot;number&quot;);
var length = document.getElementById(&quot;length&quot;);

// When the user clicks on the password field, show the message box
myInput.onfocus = function() {
  document.getElementById(&quot;message&quot;).style.display = &quot;block&quot;;
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function() {
  document.getElementById(&quot;message&quot;).style.display = &quot;none&quot;;
}

// When the user starts to type something inside the password field
myInput.onkeyup = function() {
  // Validate lowercase letters
  var lowerCaseLetters = /[a-zA-Z]/g;
  if(myInput.value.match(lowerCaseLetters)) {  
    letter.classList.remove(&quot;invalid&quot;);
    letter.classList.add(&quot;valid&quot;);
  } else {
    letter.classList.remove(&quot;valid&quot;);
    letter.classList.add(&quot;invalid&quot;);
  }
  
  // Validate capital letters
  var upperCaseLetters = /[\/\:\;\(\)\$\&amp;\@\&quot;\.\,\?\!\[\]\{\}\#\%\^\*\+\-\_\\\|\~\&lt;\> ]{1,}/g;
  if(myInput.value.match(upperCaseLetters)) {  
    capital.classList.remove(&quot;invalid&quot;);
    capital.classList.add(&quot;valid&quot;);
  } else {
    capital.classList.remove(&quot;valid&quot;);
    capital.classList.add(&quot;invalid&quot;);
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if(myInput.value.match(numbers)) {  
    number.classList.remove(&quot;invalid&quot;);
    number.classList.add(&quot;valid&quot;);
  } else {
    number.classList.remove(&quot;valid&quot;);
    number.classList.add(&quot;invalid&quot;);
  }
  
  // Validate length
  if(myInput.value.length >= 8) {
    length.classList.remove(&quot;invalid&quot;);
    length.classList.add(&quot;valid&quot;);
  } else {
    length.classList.remove(&quot;valid&quot;);
    length.classList.add(&quot;invalid&quot;);
  }
}

                    
                    
                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-wrapper&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-12&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-wrapper']/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
                	
/* Style the container for inputs */
.container {
  background-color: #f1f1f1;
  padding: 20px;
}

/* The message box is shown when the user clicks on the password field */
#message {
  display:none;
  background: #f1f1f1;
  color: #000;
  position: relative;
  padding: 20px;
  margin-top: 10px;
}

#message p {
  padding: 10px 35px;
  font-size: 18px;
}

/* Add a green text color and a checkmark when the requirements are right */
.valid {
  color: green;
}

.valid:before {
  position: relative;
  left: -35px;
  content: &quot;✔&quot;;
}

/* Add a red text color and an &quot;x&quot; when the requirements are wrong */
.invalid {
  color: red;
}

.invalid:before {
  position: relative;
  left: -35px;
  content: &quot;✖&quot;;
}

เปลี่ยนรหัสผ่าน (Change Password)

        
        
            
            	
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                ไม่พบตัวอักษรภาษาอังกฤษ ในรหัสผ่านใหม่ของท่านNot found characters in your new password

                
                    Change Password
                
                
                
            
        
    
    





function myFunction() {
  var x = document.getElementById(&quot;newpass&quot;);
  var y = document.getElementById(&quot;renewpass&quot;);
  if (x.type === &quot;password&quot;) {
    x.type = &quot;text&quot;;
	y.type = &quot;text&quot;;
  } else {
    x.type = &quot;password&quot;;
	y.type = &quot;password&quot;;
  }
}

function check(event){
	var ew = event.which;
	
    if(33 &lt;= ew &amp;&amp; ew &lt;= 38)
        return true;
    if(40 &lt;= ew &amp;&amp; ew &lt;= 126)
        return true;
    if(ew == 39){
		alert(&quot;Don&quot; , &quot;'&quot; , &quot;t use &quot; , &quot;'&quot; , &quot; (single quote) ห้ามใช้เครื่องหมาย &quot; , &quot;'&quot; , &quot; (เขาเดี่ยว)&quot;);
        return false;
	}
	alert(&quot;กรุณาเปลี่ยนภาษา&quot;);
    return false;
}

var myInput = document.getElementById(&quot;newpass&quot;);
var letter = document.getElementById(&quot;letter&quot;);
var capital = document.getElementById(&quot;capital&quot;);
var number = document.getElementById(&quot;number&quot;);
var length = document.getElementById(&quot;length&quot;);

// When the user clicks on the password field, show the message box
myInput.onfocus = function() {
  document.getElementById(&quot;message&quot;).style.display = &quot;block&quot;;
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function() {
  document.getElementById(&quot;message&quot;).style.display = &quot;none&quot;;
}

// When the user starts to type something inside the password field
myInput.onkeyup = function() {
  // Validate lowercase letters
  var lowerCaseLetters = /[a-zA-Z]/g;
  if(myInput.value.match(lowerCaseLetters)) {  
    letter.classList.remove(&quot;invalid&quot;);
    letter.classList.add(&quot;valid&quot;);
  } else {
    letter.classList.remove(&quot;valid&quot;);
    letter.classList.add(&quot;invalid&quot;);
  }
  
  // Validate capital letters
  var upperCaseLetters = /[\/\:\;\(\)\$\&amp;\@\&quot;\.\,\?\!\[\]\{\}\#\%\^\*\+\-\_\\\|\~\&lt;\> ]{1,}/g;
  if(myInput.value.match(upperCaseLetters)) {  
    capital.classList.remove(&quot;invalid&quot;);
    capital.classList.add(&quot;valid&quot;);
  } else {
    capital.classList.remove(&quot;valid&quot;);
    capital.classList.add(&quot;invalid&quot;);
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if(myInput.value.match(numbers)) {  
    number.classList.remove(&quot;invalid&quot;);
    number.classList.add(&quot;valid&quot;);
  } else {
    number.classList.remove(&quot;valid&quot;);
    number.classList.add(&quot;invalid&quot;);
  }
  
  // Validate length
  if(myInput.value.length >= 8) {
    length.classList.remove(&quot;invalid&quot;);
    length.classList.add(&quot;valid&quot;);
  } else {
    length.classList.remove(&quot;valid&quot;);
    length.classList.add(&quot;invalid&quot;);
  }
}

                    
                    
                &quot;) or . = concat(&quot;
                	
/* Style the container for inputs */
.container {
  background-color: #f1f1f1;
  padding: 20px;
}

/* The message box is shown when the user clicks on the password field */
#message {
  display:none;
  background: #f1f1f1;
  color: #000;
  position: relative;
  padding: 20px;
  margin-top: 10px;
}

#message p {
  padding: 10px 35px;
  font-size: 18px;
}

/* Add a green text color and a checkmark when the requirements are right */
.valid {
  color: green;
}

.valid:before {
  position: relative;
  left: -35px;
  content: &quot;✔&quot;;
}

/* Add a red text color and an &quot;x&quot; when the requirements are wrong */
.invalid {
  color: red;
}

.invalid:before {
  position: relative;
  left: -35px;
  content: &quot;✖&quot;;
}

เปลี่ยนรหัสผ่าน (Change Password)

        
        
            
            	
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                ไม่พบตัวอักษรภาษาอังกฤษ ในรหัสผ่านใหม่ของท่านNot found characters in your new password

                
                    Change Password
                
                
                
            
        
    
    





function myFunction() {
  var x = document.getElementById(&quot;newpass&quot;);
  var y = document.getElementById(&quot;renewpass&quot;);
  if (x.type === &quot;password&quot;) {
    x.type = &quot;text&quot;;
	y.type = &quot;text&quot;;
  } else {
    x.type = &quot;password&quot;;
	y.type = &quot;password&quot;;
  }
}

function check(event){
	var ew = event.which;
	
    if(33 &lt;= ew &amp;&amp; ew &lt;= 38)
        return true;
    if(40 &lt;= ew &amp;&amp; ew &lt;= 126)
        return true;
    if(ew == 39){
		alert(&quot;Don&quot; , &quot;'&quot; , &quot;t use &quot; , &quot;'&quot; , &quot; (single quote) ห้ามใช้เครื่องหมาย &quot; , &quot;'&quot; , &quot; (เขาเดี่ยว)&quot;);
        return false;
	}
	alert(&quot;กรุณาเปลี่ยนภาษา&quot;);
    return false;
}

var myInput = document.getElementById(&quot;newpass&quot;);
var letter = document.getElementById(&quot;letter&quot;);
var capital = document.getElementById(&quot;capital&quot;);
var number = document.getElementById(&quot;number&quot;);
var length = document.getElementById(&quot;length&quot;);

// When the user clicks on the password field, show the message box
myInput.onfocus = function() {
  document.getElementById(&quot;message&quot;).style.display = &quot;block&quot;;
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function() {
  document.getElementById(&quot;message&quot;).style.display = &quot;none&quot;;
}

// When the user starts to type something inside the password field
myInput.onkeyup = function() {
  // Validate lowercase letters
  var lowerCaseLetters = /[a-zA-Z]/g;
  if(myInput.value.match(lowerCaseLetters)) {  
    letter.classList.remove(&quot;invalid&quot;);
    letter.classList.add(&quot;valid&quot;);
  } else {
    letter.classList.remove(&quot;valid&quot;);
    letter.classList.add(&quot;invalid&quot;);
  }
  
  // Validate capital letters
  var upperCaseLetters = /[\/\:\;\(\)\$\&amp;\@\&quot;\.\,\?\!\[\]\{\}\#\%\^\*\+\-\_\\\|\~\&lt;\> ]{1,}/g;
  if(myInput.value.match(upperCaseLetters)) {  
    capital.classList.remove(&quot;invalid&quot;);
    capital.classList.add(&quot;valid&quot;);
  } else {
    capital.classList.remove(&quot;valid&quot;);
    capital.classList.add(&quot;invalid&quot;);
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if(myInput.value.match(numbers)) {  
    number.classList.remove(&quot;invalid&quot;);
    number.classList.add(&quot;valid&quot;);
  } else {
    number.classList.remove(&quot;valid&quot;);
    number.classList.add(&quot;invalid&quot;);
  }
  
  // Validate length
  if(myInput.value.length >= 8) {
    length.classList.remove(&quot;invalid&quot;);
    length.classList.add(&quot;valid&quot;);
  } else {
    length.classList.remove(&quot;valid&quot;);
    length.classList.add(&quot;invalid&quot;);
  }
}

                    
                    
                &quot;))]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
                	
/* Style the container for inputs */
.container {
  background-color: #f1f1f1;
  padding: 20px;
}

/* The message box is shown when the user clicks on the password field */
#message {
  display:none;
  background: #f1f1f1;
  color: #000;
  position: relative;
  padding: 20px;
  margin-top: 10px;
}

#message p {
  padding: 10px 35px;
  font-size: 18px;
}

/* Add a green text color and a checkmark when the requirements are right */
.valid {
  color: green;
}

.valid:before {
  position: relative;
  left: -35px;
  content: &quot;✔&quot;;
}

/* Add a red text color and an &quot;x&quot; when the requirements are wrong */
.invalid {
  color: red;
}

.invalid:before {
  position: relative;
  left: -35px;
  content: &quot;✖&quot;;
}

เปลี่ยนรหัสผ่าน (Change Password)

        
        
            
            	
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                
                    
                

                
                    Change Password
                
                
                
            
        
    
    





function myFunction() {
  var x = document.getElementById(&quot;newpass&quot;);
  var y = document.getElementById(&quot;renewpass&quot;);
  if (x.type === &quot;password&quot;) {
    x.type = &quot;text&quot;;
	y.type = &quot;text&quot;;
  } else {
    x.type = &quot;password&quot;;
	y.type = &quot;password&quot;;
  }
}

function check(event){
	var ew = event.which;
	
    if(33 &lt;= ew &amp;&amp; ew &lt;= 38)
        return true;
    if(40 &lt;= ew &amp;&amp; ew &lt;= 126)
        return true;
    if(ew == 39){
		alert(&quot;Don&quot; , &quot;'&quot; , &quot;t use &quot; , &quot;'&quot; , &quot; (single quote) ห้ามใช้เครื่องหมาย &quot; , &quot;'&quot; , &quot; (เขาเดี่ยว)&quot;);
        return false;
	}
	alert(&quot;กรุณาเปลี่ยนภาษา&quot;);
    return false;
}

var myInput = document.getElementById(&quot;newpass&quot;);
var letter = document.getElementById(&quot;letter&quot;);
var capital = document.getElementById(&quot;capital&quot;);
var number = document.getElementById(&quot;number&quot;);
var length = document.getElementById(&quot;length&quot;);

// When the user clicks on the password field, show the message box
myInput.onfocus = function() {
  document.getElementById(&quot;message&quot;).style.display = &quot;block&quot;;
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function() {
  document.getElementById(&quot;message&quot;).style.display = &quot;none&quot;;
}

// When the user starts to type something inside the password field
myInput.onkeyup = function() {
  // Validate lowercase letters
  var lowerCaseLetters = /[a-zA-Z]/g;
  if(myInput.value.match(lowerCaseLetters)) {  
    letter.classList.remove(&quot;invalid&quot;);
    letter.classList.add(&quot;valid&quot;);
  } else {
    letter.classList.remove(&quot;valid&quot;);
    letter.classList.add(&quot;invalid&quot;);
  }
  
  // Validate capital letters
  var upperCaseLetters = /[\/\:\;\(\)\$\&amp;\@\&quot;\.\,\?\!\[\]\{\}\#\%\^\*\+\-\_\\\|\~\&lt;\> ]{1,}/g;
  if(myInput.value.match(upperCaseLetters)) {  
    capital.classList.remove(&quot;invalid&quot;);
    capital.classList.add(&quot;valid&quot;);
  } else {
    capital.classList.remove(&quot;valid&quot;);
    capital.classList.add(&quot;invalid&quot;);
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if(myInput.value.match(numbers)) {  
    number.classList.remove(&quot;invalid&quot;);
    number.classList.add(&quot;valid&quot;);
  } else {
    number.classList.remove(&quot;valid&quot;);
    number.classList.add(&quot;invalid&quot;);
  }
  
  // Validate length
  if(myInput.value.length >= 8) {
    length.classList.remove(&quot;invalid&quot;);
    length.classList.add(&quot;valid&quot;);
  } else {
    length.classList.remove(&quot;valid&quot;);
    length.classList.add(&quot;invalid&quot;);
  }
}

                    
                    
                &quot;) or . = concat(&quot;
                	
/* Style the container for inputs */
.container {
  background-color: #f1f1f1;
  padding: 20px;
}

/* The message box is shown when the user clicks on the password field */
#message {
  display:none;
  background: #f1f1f1;
  color: #000;
  position: relative;
  padding: 20px;
  margin-top: 10px;
}

#message p {
  padding: 10px 35px;
  font-size: 18px;
}

/* Add a green text color and a checkmark when the requirements are right */
.valid {
  color: green;
}

.valid:before {
  position: relative;
  left: -35px;
  content: &quot;✔&quot;;
}

/* Add a red text color and an &quot;x&quot; when the requirements are wrong */
.invalid {
  color: red;
}

.invalid:before {
  position: relative;
  left: -35px;
  content: &quot;✖&quot;;
}

เปลี่ยนรหัสผ่าน (Change Password)

        
        
            
            	
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                
                    
                

                
                    Change Password
                
                
                
            
        
    
    





function myFunction() {
  var x = document.getElementById(&quot;newpass&quot;);
  var y = document.getElementById(&quot;renewpass&quot;);
  if (x.type === &quot;password&quot;) {
    x.type = &quot;text&quot;;
	y.type = &quot;text&quot;;
  } else {
    x.type = &quot;password&quot;;
	y.type = &quot;password&quot;;
  }
}

function check(event){
	var ew = event.which;
	
    if(33 &lt;= ew &amp;&amp; ew &lt;= 38)
        return true;
    if(40 &lt;= ew &amp;&amp; ew &lt;= 126)
        return true;
    if(ew == 39){
		alert(&quot;Don&quot; , &quot;'&quot; , &quot;t use &quot; , &quot;'&quot; , &quot; (single quote) ห้ามใช้เครื่องหมาย &quot; , &quot;'&quot; , &quot; (เขาเดี่ยว)&quot;);
        return false;
	}
	alert(&quot;กรุณาเปลี่ยนภาษา&quot;);
    return false;
}

var myInput = document.getElementById(&quot;newpass&quot;);
var letter = document.getElementById(&quot;letter&quot;);
var capital = document.getElementById(&quot;capital&quot;);
var number = document.getElementById(&quot;number&quot;);
var length = document.getElementById(&quot;length&quot;);

// When the user clicks on the password field, show the message box
myInput.onfocus = function() {
  document.getElementById(&quot;message&quot;).style.display = &quot;block&quot;;
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function() {
  document.getElementById(&quot;message&quot;).style.display = &quot;none&quot;;
}

// When the user starts to type something inside the password field
myInput.onkeyup = function() {
  // Validate lowercase letters
  var lowerCaseLetters = /[a-zA-Z]/g;
  if(myInput.value.match(lowerCaseLetters)) {  
    letter.classList.remove(&quot;invalid&quot;);
    letter.classList.add(&quot;valid&quot;);
  } else {
    letter.classList.remove(&quot;valid&quot;);
    letter.classList.add(&quot;invalid&quot;);
  }
  
  // Validate capital letters
  var upperCaseLetters = /[\/\:\;\(\)\$\&amp;\@\&quot;\.\,\?\!\[\]\{\}\#\%\^\*\+\-\_\\\|\~\&lt;\> ]{1,}/g;
  if(myInput.value.match(upperCaseLetters)) {  
    capital.classList.remove(&quot;invalid&quot;);
    capital.classList.add(&quot;valid&quot;);
  } else {
    capital.classList.remove(&quot;valid&quot;);
    capital.classList.add(&quot;invalid&quot;);
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if(myInput.value.match(numbers)) {  
    number.classList.remove(&quot;invalid&quot;);
    number.classList.add(&quot;valid&quot;);
  } else {
    number.classList.remove(&quot;valid&quot;);
    number.classList.add(&quot;invalid&quot;);
  }
  
  // Validate length
  if(myInput.value.length >= 8) {
    length.classList.remove(&quot;invalid&quot;);
    length.classList.add(&quot;valid&quot;);
  } else {
    length.classList.remove(&quot;valid&quot;);
    length.classList.add(&quot;invalid&quot;);
  }
}

                    
                    
                &quot;))]</value>
   </webElementXpaths>
</WebElementEntity>
