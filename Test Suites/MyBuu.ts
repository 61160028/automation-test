<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MyBuu</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>b31b4dc8-3aa9-434a-be8d-cd1bfe359d21</testSuiteGuid>
   <testCaseLink>
      <guid>7ef31092-7df2-4348-a52b-1abd33b8bdab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f8711669-5d4c-4e15-8648-f267aa6c14d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Fail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e65b4f08-5bb9-450a-a623-e43902d4a3ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change password special character</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67b274e0-523e-45f5-9c60-4c614d925c5e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change password no number</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bebc77b7-d3a8-4dc7-99c7-845059a485df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change password no character</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a90c508d-a445-40f6-8bd6-a2711b69eecc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change pass fail no 8 length</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58007fe5-5341-4387-9b5a-b6072c7511fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change password success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9446f0c-1e68-4be5-ba88-808f08c1eab3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Logout success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
